

Iniciando o Gitlab

```shell
 docker-compose -f docker-compose-gitlab.yml up -d
```


Adicione no hosts do seu Sistema Operacional
```
127.0.0.1 gitlab.phprs.com.br
# OU 
# 192.168.33.10 gitlab.phprs.com.br
```



## Proximos passos

Arquitetura
O CI/CD do GitLab faz parte do GitLab, um aplicativo da Web com uma API que armazena seu estado em um banco de dados. Ele gerencia projetos / constrói e fornece uma ótima interface de usuário, além de todos os recursos do GitLab.

GitLab Runner é uma aplicação que processa compilações. Ele pode ser implantado separadamente e funciona com o GitLab CI / CD por meio de uma API.

Para executar testes, você precisa de pelo menos uma instância do GitLab e um GitLab Runner.

 - Criar usuário deployer
 - Instalar o gitlab runner
 - Registre o novo gitlab runner
 - Criar arquivo .gitlab-ci.yml


### Criar usuário deployer
Primeiro de tudo, ssh no seu servidor remoto:

```shell
ssh root@192.168.33.30
```

Criar o novo usuário

```shell
sudo adduser deployer 
sudo usermod -g www-data deployer
```

Depois disso, você efetua login neste usuário para criar chaves ssh.

```shell
ssh deployer@192.168.33.30
```
Após o login bem-sucedido, gere chaves ssh e copie o caminho para a chave privada. 
Se precisar de mais informações, consulte este artigo 
https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2 
No nosso caso, pode ser algo como isto:

```shell
ssh-keygen
cat /home/deployer/.ssh/id_rsa
```

## Instalar o gitlab runner

No GitLab, os Runners executam os trabalhos que você define em ´.gitlab-ci.yml´. 
Um Runner pode ser uma máquina virtual, um VPS, uma máquina bare-metal, um contêiner 
de encaixe ou mesmo um cluster de contêineres. 
O GitLab e os Runners se comunicam por meio de uma API; portanto, o único requisito 
é que a máquina do Runners tenha acesso à rede do servidor GitLab.

### Para instalar o Runner:

SSH no seu servidor remoto

```shell
ssh deployer@192.168.33.30
```

## Ubuntu
Adicione o repositório oficial do GitLab:

```shell
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
```

Instalar o gitlab runner
```shell
sudo apt-get install gitlab-runner
```

## OU Linux x86-64 

```shell
sudo curl -L --output /usr/local/bin/gitlab-runner \
        https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```

Conceda permissões para executar:

```shell
sudo chmod +x /usr/local/bin/gitlab-runner
```

Instalar o gitlab runner
```shell
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo mkdir -p /home/gitlab-runner
sudo chmod 755 /etc/gitlab-runner/config.toml
sudo gitlab-runner start
```

Registre o novo runner do gitlab

```shell
sudo gitlab-runner register
```
OU

```shell
sudo gitlab-runner register \
            --non-interactive \
            --executor ssh  \
            --url 'http://gitlab.phprs.com.br:30080/ci' \
            --registration-token 'ELa5Ke-UUF2aQUcz-tRt' \
            --ssh-host 192.168.33.30 \
            --ssh-port 22 \
            --ssh-identity-file /home/vagrant/.ssh/id_rsa \
            --ssh-user deployer \
            --ssh-password gitlab.phprs
```

### Configuração avançada

A configuração do GitLab Runner usa o formato TOML.

```shell
sudo vi /etc/gitlab-runner/config.toml
```

## Criar arquivo .gitlab-ci.yml

Os pipelines de CI/CD do GitLab são configurados usando um arquivo YAML chamado `.gitlab-ci.yml` em cada projeto.

O arquivo `.gitlab-ci.yml` define a estrutura e a ordem dos pipelines e determina:

### O que executar usando o GitLab Runner.

Que decisões tomar quando condições específicas são encontradas. 
Por exemplo, quando um processo é bem-sucedido ou falha.

```shell
cat ~/.ssh/id_rsa.pub | ssh deployer@192.168.33.30 'cat >> ~/.ssh/authorized_keys'
```














Referencias

Install GitLab with Docker
https://www.linode.com/docs/development/version-control/install-gitlab-with-docker/

Easy Gitlab installer for Ubuntu - Vagrant
https://github.com/tuminoid/gitlab-installer

A step-by-step guide to running GitLab CE in Docker
https://developer.ibm.com/code/2017/07/13/step-step-guide-running-gitlab-ce-docker/

Install GitLab Runner manually on GNU/Linux
https://docs.gitlab.com/runner/install/linux-manually.html

GitLab CI/CD Pipeline Configuration Reference
https://docs.gitlab.com/ee/ci/yaml/README.html

GitLab CI/CD environment variables
https://docs.gitlab.com/ee/ci/variables/

Predefined environment variables reference
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html