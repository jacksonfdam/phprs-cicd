#!/usr/bin/env bash

if [ ! -f ~/runonce ]
    then

        sudo su

        echo
        echo "Provisioning virtual machine..."
        echo

        # System Variables
        HOSTNAME='192.168.33.30'
        COMPOSER_ALLOW_SUPERUSER=1
        COMPOSER_MEMORY_LIMIT=-1

        INSTALLATION_RANDOM_PASSWORDS_FILE=/tmp/installation-random-passwords
        [[ -f ${INSTALLATION_RANDOM_PASSWORDS_FILE} ]] && INSTALLATION_RANDOM_PASSWORDS_FILE_EXISTS=1 || INSTALLATION_RANDOM_PASSWORDS_FILE_EXISTS=0
        [[ ${INSTALLATION_RANDOM_PASSWORDS_FILE_EXISTS} = 0 ]] && touch ${INSTALLATION_RANDOM_PASSWORDS_FILE}

        [[ ${INSTALLATION_RANDOM_PASSWORDS_FILE_EXISTS} = 0 ]] && echo "ROOT_PASS=`date +%s | sha256sum | base64 | head -c 64`" >> /tmp/installation-random-passwords

        source ${INSTALLATION_RANDOM_PASSWORDS_FILE}

        ROOT_BACKUP_DATABASE_USER=${ROOT_PASS}

        export DEBIAN_FRONTEND=noninteractive

        echo
        echo "Prepeare essentials"
        echo

        locale-gen UTF-8

        apt-get update -y
        apt-get upgrade -y

        apt-get -q -y install build-essential zip unzip curl wget software-properties-common cron vim

        echo
        echo "Installing Git"
        echo
        apt-get install git -y > /dev/null

        echo
        echo "Installing Apache"
        echo
        apt-get install -y apache2
        a2enmod rewrite
        service apache2 restart
        systemctl stop apache2.service
        sed -i -e 's/Listen.*/Listen 88/g' /etc/apache2/ports.conf
        cp -PR /home/vagrant/src/apache2/000-default.conf /etc/apache2/sites-available/000-default.conf
        cat /dev/null > /var/www/index.html

        echo
        echo "Installing PHP"
        echo
        apt-get install -y php7.0-mcrypt php7.0-mbstring php php-mysql php-pear libapache2-mod-php php-curl php-imagick php-sqlite3 ffmpeg
        apt-get install -y zlibc php-soap php-intl php-gettext php-memcache php-memcached php-tidy php-geoip php-zip
        phpenmod mcrypt
        phpdismod opcache
        echo "short_open_tag = On" >> /etc/php/7.0/apache2/php.ini
        echo "date.timezone = America/Sao_Paulo" > /etc/php/7.0/apache2/php.ini
        pear channel-discover pear.phing.info
        pear install -Z phing/phing

        echo
        echo "Install Composer"
        echo
        curl -sS https://getcomposer.org/installer | php 
        mv composer.phar /usr/local/bin/composer
        composer global require hirak/prestissimo --no-interaction

        echo
        echo "Installing Mysql"
        echo
        apt-get install -y mysql-server mysql-client
        sed -i -e 's/#sql_mode.*/sql_mode=""/g' /etc/mysql/my.cnf
        sed -i -e 's/max_allowed_packet.*/#max_allowed_packet/g' /etc/mysql/my.cnf
        sed -i -e 's/mysql -u root -p mysql/mysql -u root -p${mysql_pass} mysql/g'  /var/www/stalker_portal/deploy/build.xml
        echo "[mysqld]" > /etc/mysql/conf.d/phprs.cnf
        echo "sql_mode=\"\"" >> /etc/mysql/conf.d/phprs.cnf
        echo "max_allowed_packet = 256M" >> /etc/mysql/conf.d/phprs.cnf
        sed -i -e 's/bind-address.*/bind-address = 0.0.0.0/g' /etc/mysql/my.cnf
        systemctl restart mysql

        echo
        echo "Installing Nodejs"
        echo
        apt-get install -y nodejs npm
        npm install -g npm@2.15.11
        ln -s /usr/bin/nodejs /usr/bin/node

        echo
        echo "Installing Memcached"
        echo
        apt-get install -y memcached
        sed -i -e 's/-m 64/-m 1024/g' /etc/memcached.conf
        sed -i -e 's/-l/#-l/g' /etc/memcached.conf
        systemctl restart memcached

        echo
        echo "Installing nginx"
        echo
        apt-get install -y nginx
        systemctl stop nginx
        cp -PR /home/vagrant/src/nginx/default /etc/nginx/sites-available/default

        echo
        echo "Prepare Build"
        echo
        systemctl restart apache2 nginx mysql
        supervisorctl update

        echo "Finish installing broken packages"
        apt-get install -f -y
        apt-get autoremove -y

        echo "Done."
        touch ~/runonce
fi    